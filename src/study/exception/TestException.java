//if you type into too large a value for an integer, you get InputMismatchException

//an unchecked exception need not to be handle by user,
//but a checked exception (even thrown by user) has to be handled properly, 
//either be surrounded by try{} catach{} block or be thrown back.
//This is mean that whenever you define a method throwing a check exception,
//you have to add throw ... exception after the name of the function. 
//It let the compiler know that you don't want to handle the exception but
//instead want to throw it. When you throw an exception, that means you don't
//know how to deal with it. But if it's an unchecked exception, you must
//let the compiler that you have done something about it. The only way is to 
//throw the exception back (to the method call it). 
package study.exception;

import java.nio.charset.CharacterCodingException;
import java.util.Scanner;

public class TestException {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Please type in an integer:");
		int a = in.nextInt();
		System.out.println("The value you typed in was: " + a);
	}
	
	public static void fun(int index){
		if(index<0){
			throw new RuntimeException();
		}
	}
}
