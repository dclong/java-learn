package study.concurrency.atomicsum;

import java.util.concurrent.atomic.AtomicInteger;

public class SumRunnable implements Runnable {
	private AtomicInteger sum;
	private int addend;
	
	public SumRunnable(AtomicInteger sum, int addend){
		this.sum = sum;
		this.addend = addend;
	}
	
	public void run(){
		sum.getAndAdd(addend);
	}
}
