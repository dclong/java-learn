package study.concurrency.atomicsum;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class TestSumRunnable {
	public static void main(String[] args) {
		for (int step = 0; step < 10000; ++step) {
			AtomicInteger sum = new AtomicInteger(0);
			int coresNumber = Runtime.getRuntime().availableProcessors();
			ExecutorService pool = Executors.newFixedThreadPool(coresNumber);
			for (int i = 0; i < 1000; ++i) {
				pool.execute(new SumRunnable(sum, i));
			}
			pool.shutdown();
			while (!pool.isTerminated()) {
				try {
					pool.awaitTermination(1000, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (sum.get() != 499500) {
				System.err.println("Wrong!");
			}
			System.out.println("step " + step + " done.");
		}
	}
}
