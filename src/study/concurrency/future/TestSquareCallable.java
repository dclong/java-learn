package study.concurrency.future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class TestSquareCallable {

	public static void main(String[] args) {
		for (int step = 0; step < 10000; ++step) {
			int coresNumber = Runtime.getRuntime().availableProcessors();
			ExecutorService pool = Executors.newFixedThreadPool(coresNumber);
			List<Future<Double>> list = new ArrayList<Future<Double>>();
			for (int i = 0; i < 20000; i++) {
				Callable<Double> worker = new SquareCallable(i);
				Future<Double> submit = pool.submit(worker);
				list.add(submit);
			}
			// Now retrieve the result
			for (int i = list.size() - 1; i >= 0; --i) {
				Double value;
				try {
					value = list.get(i).get();
					if (value != i * i) {
						System.err.println("1 Wrong!");
						System.out.println(value);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			list.clear();
			for (int i = 10; i < 10000; i++) {
				Callable<Double> worker = new SquareCallable(i);
				Future<Double> submit = pool.submit(worker);
				list.add(submit);
			}
			// Now retrieve the result
			for (int i = 0; i < list.size(); ++i) {
				Double value;
				try {
					value = list.get(i).get();
					if (value != (i + 10) * (i + 10)) {
						System.err.println("2 Wrong!");
						System.out.println(value);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			pool.shutdown();
//			System.out.println("step " + step);
		}
		System.out.println("Done!");
	}
}
