package study.concurrency.future;

import java.util.concurrent.Callable;

public class SquareCallable implements Callable<Double> {
	private double value;
	public SquareCallable(double value){
		this.value = value;
	}
	@Override
	public Double call() throws Exception{
		return value*value;
	}
}
