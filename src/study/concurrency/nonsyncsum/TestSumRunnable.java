package study.concurrency.nonsyncsum;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class TestSumRunnable {
	public static void main(String[] args) {
		int wrong = 0;
		int n = 10000;
		for (int step = 0; step < n; ++step) {
			NonSyncInteger sum = new NonSyncInteger(0);
			int coresNumber = Runtime.getRuntime().availableProcessors();
			ExecutorService pool = Executors.newFixedThreadPool(coresNumber);
			for (int i = 0; i < 1000; ++i) {
				pool.execute(new SumRunnable(sum, i));
			}
			pool.shutdown();
			while (!pool.isTerminated()) {
				try {
					pool.awaitTermination(1000, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (sum.getValue() != 499500) {
				wrong ++;
				System.out.println(sum.getValue());
			}
		}
		System.out.println("wrong: "+ wrong + " out of " + n + " iterations");
	}
}
