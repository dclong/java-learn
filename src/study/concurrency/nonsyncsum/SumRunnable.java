package study.concurrency.nonsyncsum;


public class SumRunnable implements Runnable{
	private NonSyncInteger sum;
	private int addend;
//	private ReentrantLock lock;
	public SumRunnable(NonSyncInteger sum, int addend){
		this.sum = sum;
		this.addend = addend;
//		lock = new ReentrantLock();
	}
	
	public void run(){//no sync, verify that this is problematic
		sum.add(addend);
	}
}
