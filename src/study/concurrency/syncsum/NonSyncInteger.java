package study.concurrency.syncsum;

public class NonSyncInteger {
	private int value;
	
	public NonSyncInteger(int value){
		this.value = value;
	}
	
	public synchronized int add(int addend){
		value += addend;
		return value;
	}
	
	public int getValue(){
		return value;
	}
}
