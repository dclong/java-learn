package study.concurrency.array;

public class SetElementRunnable implements Runnable{
	private int[] array;
	private int startIndex;
	private int endIndex;
	public SetElementRunnable(int[] array, int index){
		this(array,index,index+1);
	}
	
	public SetElementRunnable(int[] array, int startIndex, int endIndex){
		if(startIndex>=endIndex){
			System.err.println("endIndex must be greater than startIndex.");
		}
		this.array = array;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}
	
	public void run(){
		for(int index=startIndex; index<endIndex; ++index){
			for(int i=0; i<1000000000; ++i){
				for(int j=0; j<100000000; ++j){
					Math.sqrt(i*j-0.5);
				}
			}
			array[index] = 0;
		}
	}
}
