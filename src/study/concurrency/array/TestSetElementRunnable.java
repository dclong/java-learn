/**
 * An example of assign jobs manually. 
 * Jobs are assigned to threads in a balanced way. 
 * If there are k extra jobs (k<# of threads), 
 * they are assigned to the first k threads.
 */
package study.concurrency.array;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


//import dclong.util.Timer;

public class TestSetElementRunnable {
	private static int[] array = new int[100000];

	public static void main(String[] args) {
		for (int count = 0; count < 10000; ++count) {
			Arrays.fill(array, 1);
//			Timer timer = new Timer();
//			timer.start();
			for (int i = array.length - 1; i >= 0; --i) {
				new SetElementRunnable(array, i).run();
			}
//			timer.end();
			checkResult();
//			timer.printSeconds("setting elements sequentially");
			Arrays.fill(array, 1);
			int coresNumber = Runtime.getRuntime().availableProcessors();
			ExecutorService pool = Executors.newFixedThreadPool(coresNumber);
//			timer.start();
			for (int i = array.length - 1; i >= 0; --i) {
				pool.execute(new SetElementRunnable(array, i));
			}
			pool.shutdown();
			while (!pool.isTerminated()) {
				try {
					pool.awaitTermination(1000, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.out
							.println("Some thread in the pool was interrupted.");
				}
			}
//			timer.end();
			checkResult();
//			timer.printSeconds("setting elements using thread pool");
			Arrays.fill(array, 1);
//			timer.begin();
			runJobs(coresNumber);
//			timer.end();
			checkResult();
//			timer.printSeconds("setting elements using user-managed threads");
//			System.out.println("step " + count + " done.");
		}
		System.out.println("Done! All right!");
	}
	
	private static void checkResult(){
		if(dclong.util.Arrays.sum(array) != 0){
			System.err.println("Wrong!");
		}
	}
	private static void runJobs(int threadsNumber) {
		Thread[] t = new Thread[threadsNumber];
		int each = array.length / threadsNumber;
		int eachPlusOne = each + 1;
		int extra = array.length - threadsNumber * each;
		int startIndex = 0;
		int endIndex;
		for (int i = 0; i < extra; ++i) {
			endIndex = startIndex + eachPlusOne;
			t[i] = new Thread(new SetElementRunnable(array, startIndex,
					endIndex));
			t[i].start();
			startIndex = endIndex;
		}
		for (int i = extra; i < threadsNumber; ++i) {
			endIndex = startIndex + each;
			t[i] = new Thread(new SetElementRunnable(array, startIndex,
					endIndex));
			t[i].start();
			startIndex = endIndex;
		}
		for (int i = threadsNumber - 1; i >= 0; --i) {
			try {
				t[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("Thread " + i + " was interrupted.");
			}
		}
	}
}
