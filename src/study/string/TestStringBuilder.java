//Methods return the same reference, i.e. the original 
//StringBuilder object is mutated. 
package study.string;

public class TestStringBuilder {
	public static void main(String[] args){
		StringBuilder builder = new StringBuilder("abcdefg");
		System.out.println(builder);
		builder.append("1111");
		System.out.println(builder);
		builder.deleteCharAt(0);
		System.out.println(builder);
		builder.delete(0, 6);
		System.out.println(builder);
		System.out.println(builder.capacity());
		System.out.println(builder.length());
		
	}
}
