/**
 * String is an immutable class, which means that it's passed by reference. 
 * However you cannot change the state of an instance. When you try to do that, 
 * you create a new object, instead of changing the original one. 
 * 
 * It also seems that String objects with the same content also have the same address, 
 * which means that we can use == to compare two String objects instead of using method 
 * equals. However, this is not the case. Not true!!! Other methods can return a string 
 * with the same content but with a different reference.
 * 
 * "\n" is a length 1 string
 */

package study.string;

public class TestString {
	public static void main(String[] args){
		String str = "\n";
		System.out.println(str.length());
//		System.out.println(str.substring(0,str.lastIndexOf(".")));
//		System.out.println(str.substring(str.lastIndexOf(".")));
//		System.out.println("abc"+null);
//		//check reference
//		String aStr = "I'm good!";
//		String bStr = aStr;
//		System.out.println(aStr);
//		System.out.println(bStr);
//		System.out.println(aStr==bStr);
//		bStr = bStr + " ...";
//		System.out.println(aStr);
//		System.out.println(bStr);
//		System.out.println(aStr==bStr);
//		aStr += ".";
//		aStr += ".";
//		aStr += ".";
//		System.out.println(aStr==bStr);
//		//check immutable
//		bStr = "I'm OK.";
//		System.out.println(aStr);
//		System.out.println(bStr);
//		//check same address for same content
//		String cStr = "I'm good!";
//		System.out.println(cStr);
//		System.out.println(aStr==cStr);
//		System.out.println(aStr.equals(cStr));
//		
//		//yet another str
//		String dStr = "I'm OK.";
//		System.out.println(dStr);
//		System.out.println(bStr==dStr);
//		System.out.println(bStr.equals(dStr));
//		
//		//more stri 
//		System.out.println("More ...");
//		String eStr = "I'm" + " " + "OK.";
//		System.out.println(eStr);
//		System.out.println(eStr==dStr);
//		System.out.println(eStr.equals(dStr));
	}
}
