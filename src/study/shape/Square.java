package study.shape;

public class Square {
	private double sideLength;
	public Square(double sideLength){
		this.sideLength=sideLength;
	}
	public Square(){
		this(0);
	}
	public void setSideLength(double sideLength){
		this.sideLength=sideLength;
	}
	public double getSideLength(){
		return sideLength;
	}
	public double getArea(){
		return sideLength*sideLength;
	}
}
