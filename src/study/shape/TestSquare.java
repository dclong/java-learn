package study.shape;

public class TestSquare {
	public static void main(String[] args){
		Square mySquare= new Square();
		System.out.print("The side length of the square is:");
		System.out.println(mySquare.getSideLength());
		System.out.print("The area of the square is:");
		System.out.println(mySquare.getArea());
		mySquare.setSideLength(5.2);
		System.out.print("The new side length of the square is:");
		System.out.println(mySquare.getSideLength());
		System.out.print("The new Area of the square is:");
		System.out.println(mySquare.getArea());
		System.out.println(mySquare.toString());
	}
}
