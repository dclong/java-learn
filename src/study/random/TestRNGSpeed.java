
package study.random;

import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import dclong.util.Timer;

public class TestRNGSpeed {
	public static void main(String[] args){
		//CommonMath
		RandomGenerator rg = new Well44497b(29);
		RandomDataImpl rng = new RandomDataImpl(rg);
		Timer timer = new Timer();
		timer.start();
		for(long i=0; i<130000000; ++i){
//			rng.nextPermutation(16, 8);
//			rng.nextGaussian(0, 1);
			rng.nextInt(0, 65779);
		}
		timer.stop();
		timer.printSeconds("generating random numbers");
	}
}
