package study.random;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import dclong.util.Timer;


/**
 * Test the speed of generating random permutations using parallel computing.
 * 
 * @author adu
 * 
 */
public class TestSpeed {
	public static void main(String[] args) {
		RandomGenerator rg = new Well44497b();
		RandomDataImpl rng = new RandomDataImpl(rg);
		Timer timer = new Timer();
		timer.begin();
		final int MAX_THREADS = Runtime.getRuntime().availableProcessors();
		ExecutorService pool = Executors.newFixedThreadPool(MAX_THREADS);
		for (int i = 0; i < 10000; i++) {
			pool.execute(new RandomPermutationRunnable(rng, 2869, 330));
		}
		pool.shutdown();
		while (!pool.isTerminated()) {
			try {
				pool.awaitTermination(1000, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		timer.end();
		timer.printSeconds("generating random numbers in parallel");
		timer.begin();
		for (int i = 0; i < 10000; i++) {
//			rng.nextPermutation(2869, 330);
			for (int j = 0; j < 100000; j++) {
				Math.pow(j, 8);
			}
		}
		timer.end();
		timer.printSeconds("generating random numbers in serial");
	}
}
