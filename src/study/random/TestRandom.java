package study.random;

import org.apache.commons.math3.random.*;
/**
 * Examine the quality of the Well44497b random number generator.  
 * @author adu
 *
 */
public class TestRandom {
	public static void main(String[] args){
		RandomGenerator rg = new Well44497b();
		RandomDataImpl rng = new RandomDataImpl(rg);
		int rintsNumber = 100000;
		int rints[] = new int[rintsNumber];
		for(int i=0; i<rintsNumber; i++){
			rints[i] = rng.nextInt(0, 9);
		}
		//count the frequency of each integers
		int freq[] = new int[10];
		for(int i=0; i<rintsNumber; i++){
			freq[rints[i]]++;
		}
		//print out the frequency
		for(int i=0; i<10; i++){
			System.out.println(i+": "+freq[i]);
		}
	}
}
