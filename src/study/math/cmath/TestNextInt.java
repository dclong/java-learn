//The nextInt method of RandomDataImpl is not consistent with others. 
//You can either email the developer team to modify it or just bear it. 

package study.math.cmath;

import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;


public class TestNextInt {
	public static void main(String[] args){
		RandomGenerator rg = new Well44497b();
		RandomDataImpl rng = new RandomDataImpl(rg);
		System.out.println(rng.nextInt(0, 1));
	}
}
