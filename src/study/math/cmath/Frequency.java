package study.math.cmath;

public class Frequency {
	public final int value;
	protected int freq;
	public Frequency(int value, int freq){
		this.value = value;
		this.freq = freq;
	}
	
	public Frequency(int value){
		this(value,1);
	}
	
	public String toString(){
		return value + ": " + freq;
	}
}
