package study.math.cmath;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

public class TestRNG {
	public static void main(String[] args){
		RandomGenerator rg = new Well44497b(29);
		RandomDataImpl rng = new RandomDataImpl(rg);
		int numberOfElements = 12870;
		int upper = numberOfElements - 1;
		int[] freq = new int[numberOfElements];
		Arrays.fill(freq, 0);
		int averageFreq = 1000;
		for(int i=0; i<numberOfElements; ++i){
			for(int j=0; j<averageFreq; ++j){
				freq[rng.nextInt(0, upper)]++;
			}
		}
		Arrays.sort(freq);
		ArrayList<Frequency> hist = new ArrayList<Frequency>(100);
		Frequency currentFrequency = new Frequency(-1);
		for(int i=0; i<numberOfElements; ++i){
			if(freq[i]!=currentFrequency.value){
				currentFrequency = new Frequency(freq[i]);
				hist.add(currentFrequency);
			}else{
				currentFrequency.freq++;
			}
		}
		int size = hist.size();
		for(int i=0; i<size; ++i){
			System.out.println(hist.get(i));
		}
	}
}
