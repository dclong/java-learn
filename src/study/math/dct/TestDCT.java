package study.math.dct;

//import java.util.Scanner;

public class TestDCT {
	public static void main(String[] args){
		//Scanner in = new Scanner(args[0]);
		//int n = in.nextInt();
		long n=1000000000;
		double sum=0;
		double logn=Math.log(n);
		for(long i = 1; i <= n; i++){
			sum+=Math.pow(Math.E, -(i+1.)/i*logn);
		}
		System.out.println("The sum is "+sum);
	}
}
