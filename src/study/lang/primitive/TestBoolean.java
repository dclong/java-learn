//Boolean expressions are evaluated from left to right!
package study.lang.primitive;

public class TestBoolean {
	public static void main(String[] args){
		int[] aArray = {0,1,2,3};
		int index = -1;
		System.out.println(index>=0&&aArray[index]>1);
		System.out.println(aArray[index]>1&&index>=0);
	}
}
