//Casting have high priority than arithmetic operations.
//Casting won't happen until required and won't automatically expand to other place.
//Notice that the how write mathematical expression might affect
//behavior of casting, and thus the final result.
//double value can be represented by adding a d to the end of the integer value.
package study.lang.cast;

public class TestCast {
	public static void main(String[] args){
		System.out.println(1/2);
		System.out.println((double)1/2);
		System.out.println(0.2*5+6/12);
		System.out.println(3.0*2/3);
		System.out.println(200d);
		System.out.println(200);
		System.out.println(1/2d);
		System.out.println((int)-2.999999);
	}
}
