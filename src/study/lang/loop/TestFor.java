//The expression in the test condition part is re-calculated every time. 
//This is different from some other programming languages, e.g. R and Matlab.
//The expression in the initial condition part is calculated only once.

package study.lang.loop;

import java.util.ArrayList;

public class TestFor {
	public static void main(String[] args){
		ArrayList<Integer> aArrayList = new ArrayList<Integer>();
		aArrayList.add(1);
		aArrayList.add(2);
		aArrayList.add(3);
//		for(int i=0; i<aArrayList.size(); i++){
//			if(i>10){
//				break;
//			}
//			System.out.println(aArrayList.get(i));
//			aArrayList.add(i);
//		}
		for(int i=aArrayList.size()-1; i>=0; i--){
			if(i>10){
				break;
			}
			System.out.println(aArrayList.get(i));
			aArrayList.add(i);
		}
		//
		int size = 3;
		for(int i=0; i<size; i++){
			if(i>10){
				break;
			}
			System.out.println(i);
			size++;
		}
	}
}
