//The test condition in loop is check very iteration, and every change of the variables involved 
//in the test condition takes effect. 
//For the case where the test condition doesn't change, will Java optimize the code?
 
package study.lang.loop;

public class TestLoop {
	public static void main(String[] args){
		int size = 10;
		for(int i=0; i<size; ++i){
			System.out.println(i+"\n");
			--size;
		}
	}
}
