package study.lang.keyword;
import org.apache.commons.math3.random.RandomDataImpl;

public class FinalClass {
	private final RandomDataImpl rng;
	
	private FinalClass(Builder builder){
		rng = builder.rng;
	}
	
	public static class Builder{
		private final RandomDataImpl rng;
		
		public Builder(RandomDataImpl aRNG){
			rng = aRNG;
		}
		
		public FinalClass builder(){
			return new FinalClass(this);
		}
	}
	
	public RandomDataImpl getRNG(){
		return rng;
	}
	
}
