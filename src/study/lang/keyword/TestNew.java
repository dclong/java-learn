//It seems that to create a big array is slower than to create multiple smaller 
//arrays with the same total length. This might because it's harder to find big 
//continuous blocks of memory while it's easy to find smaller continuous blocks 
//memory.
//The above is not quite right. It depends on different systems. 
//...
package study.lang.keyword;

public class TestNew {
	public static void main(String[] args){
		int step = 1000000;
		long startTime = System.currentTimeMillis();
//		for(int i=0; i<step; i++){
//			double[] a = new double[1000];
//		}
		for(int i=0; i<step; i++){
			for(int j=0; j<100; j++){
				double[] a = new double[10];
			}
		}
		long endTime = System.currentTimeMillis();
		double usedTime = (endTime - startTime)/1000d;
		System.out.println("The time used is "+usedTime+" seconds.");
	}
}
